<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seeder extends CI_Controller 
{

	private $faker;

	public function __construct() 
	{
			parent::__construct();
			
			if(! $this->input->is_cli_request()) {
				exit("No se puede accesar desde el navegador");
			}

			if(ENVIRONMENT !== 'development'){
				exit('Solo valido para entornos de desarrollo');
			}

			$this->faker = Faker\Factory::create('es_VE');	
			
	}

	public function cargos() 
	{
		echo "Llenando la tabla cargos";

		$cargos = array(
			'Gerente Comercial',
			'Gerente de Ventas',
			'Subgerente de Ventas',
			'Supervisor de Ventas',
			'Vendedor',
			'Vendedor de Salón',
			'Promotor',
			'Administrativo Área Ventas',
			'Asistente de Ventas',
			'Preventista',
			'Asistente de Escrituraciones',
			'Gerente de Sucursal',
			'Comercial Unidad de Negocios',
			'Encargado de Local Comercial',
			'Analista de Ventas',
			'Ejecutivo de Ventas',
			'Ejecutivo de Ventas',
			'Desarrollador de Negocios',
			'Gerente de Marketing',
			'Supervisor de Marketing',
			'Ejecutivo de Marketing',
			'Gerente de Producto',
			'Investigador de Mercado',
			'Encuestador Área Marketing',
			'Analista de Marketing',
			'Analista de Marketing Junior',
			'Analista de Trade Marketing',
			'Asistente de Marketing',
			'Community Manager',
			'Encargado de Stock Comercio Minorista',
			'Merchandiser',
			'Brand Manager',
			'Gerente de Canal',
			'Supervisor de Telemarketing',
			'Telemarketero'
		);

		$this->db->truncate('tbcargo');

		for ($i=0; $i < 35; $i++) { 
			$data = array(
				'cargo'  => $this->faker->randomElement($cargos),
			);

			$this->db->insert('tbcargo', $data);
			$id = $this->db->insert_id();
			echo 'cargo ' . $id . PHP_EOL;
		}

			echo 'Fin de Seeder de Cargos';
		
	}

	public function clientes() 
	{
		echo "Llenando la tabla clientes";

		$this->db->truncate('tbpersonas');

		$cargos = array(
			'Gerente Comercial',
			'Gerente de Ventas',
			'Subgerente de Ventas',
			'Supervisor de Ventas',
			'Vendedor',
			'Vendedor de Salón',
			'Promotor',
			'Administrativo Área Ventas',
			'Asistente de Ventas',
			'Preventista',
			'Asistente de Escrituraciones',
			'Gerente de Sucursal',
			'Comercial Unidad de Negocios',
			'Encargado de Local Comercial',
			'Analista de Ventas',
			'Ejecutivo de Ventas',
			'Ejecutivo de Ventas',
			'Desarrollador de Negocios',
			'Gerente de Marketing',
			'Supervisor de Marketing',
			'Ejecutivo de Marketing',
			'Gerente de Producto',
			'Investigador de Mercado',
			'Encuestador Área Marketing',
			'Analista de Marketing',
			'Analista de Marketing Junior',
			'Analista de Trade Marketing',
			'Asistente de Marketing',
			'Community Manager',
			'Encargado de Stock Comercio Minorista',
			'Merchandiser',
			'Brand Manager',
			'Gerente de Canal',
			'Supervisor de Telemarketing',
			'Telemarketero'
		);

		for ($i=0; $i < 200; $i++) { 
			$data = [
				'cedula'    => $this->faker->unique()->randomNumber(6),
				'nombre'    => $this->faker->name,
				'sexo'      => $this->faker->randomElement(array('MASCULINO','FEMENINO')),
				'cargo_id'  => $this->faker->randomFloat(NULL, 0, 35),
				'direccion' => $this->faker->address,
				'correo'    => $this->faker->unique()->email,
				'tlfnocel'  => $this->faker->phoneNumber,
				'tlfnocas'  => $this->faker->phoneNumber,
				'statusper' => 'ACTIVO',
				'fechain'   => date('Y-m-d'),
				'operador'  => 'ANGEL'
			];

				$this->db->insert('tbpersonas',$data);
				$id = $this->db->insert_id();
				
				echo 'Persona ' . $id . PHP_EOL;
		}

			echo 'Fin del Seeder de Personas';
	}

}