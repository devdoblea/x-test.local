<!-- footer content -->
		<footer style="font-size: 10px;">
			<div class="pull-right">
				&copy;  DEVDOBLEA Todos los Derechos Reservados
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Powered By <a href="https://elcuartodeangel.wordpress.com">ANGEL EMIRO ANTUNEZ VILLASMIL</a> y
				Gentelella <a href="https://colorlib.com">Colorlib</a>
				&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
				Webmaster DEVDOBLEA @doble_a71 &copy; 2018
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>
</div>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url()?>assets/js/custom.min.js"></script>
<!--Consumiendo todos los alerts que haga el sistema-->
<script>
	function consume_alert(tipo_alert)  {
		var animate_in = $("#animate_in").val(),animate_out = $("#animate_out").val();
		var _alert;
		// si el tipo de error no esta definido entonces sera mostrado como tipo error
		if(tipo_alert == undefined){ tipo_alert = 'error'; } else { tipo_alert = tipo_alert; }
		if (_alert) return;
			_alert = window.alert;
			window.alert = function(message,tipo) {
				new PNotify({
					title: 'ALERTA..!!',
					text: message,
					type: tipo_alert,
					styling: 'bootstrap3',
								delay:2000,
								mobile:{swipe_dismiss:true,styling:true},
								desktop: {desktop: true,fallback: true},
								animate: {
										animate: true,
										in_class: animate_in,
										out_class: animate_out
								}
				});
			};
	}
</script>

	</body>
</html>